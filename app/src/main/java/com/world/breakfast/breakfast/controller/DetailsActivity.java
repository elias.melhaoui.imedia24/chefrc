package com.world.breakfast.breakfast.controller;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.squareup.picasso.Picasso;
import com.world.breakfast.breakfast.R;

import java.lang.reflect.Type;
import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import cz.msebera.android.httpclient.Header;

public class DetailsActivity extends AppCompatActivity {

    TextView name, ingredientsD, instructionsD, likes;
    ImageView image, imageFa;
    Toolbar toolbarDetail;
    Intent sendIntent;
    int id;
    String nameI, ingredientsI, instructionsI,imageI;
    int  likesI,idI;
    FloatingActionButton fab;

    boolean favorite = false;
    private FirebaseAnalytics firebaseAnalytics;

    private AdView mAdView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        MobileAds.initialize(this,
                "ca-app-pub-2805622275935178~1067157327");

        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().addTestDevice("F46327814C9DDA874999EA5C56137236").build();
        mAdView.loadAd(adRequest);

        Intent intent = getIntent();
        nameI =intent.getStringExtra("name");
        ingredientsI = intent.getStringExtra("ingredients");
        instructionsI = intent.getStringExtra("instructions");
        imageI = intent.getStringExtra("image");
        likesI = intent.getIntExtra("likes", 0);
        idI =intent.getIntExtra("id",0);

        name = findViewById(R.id.textViewTitle);
        image = findViewById(R.id.imageView6);
        likes = findViewById(R.id.tvLikes);
        instructionsD = findViewById(R.id.textViewDescription2);
        ingredientsD = findViewById(R.id.textViewDescription1);
        fab = findViewById(R.id.fab);


        name.setText(nameI);
        Picasso.get().load(imageI).into(image);
        instructionsD.setText(instructionsI);
        ingredientsD.setText(ingredientsI);
        likes.setText(String.valueOf(likesI));


        for (int i : MainActivity.favoriteList) {
            if (i == getIntent().getIntExtra("id", -1)) {
                favorite = true;
                fab.setImageResource(R.drawable.ic_favorite_black_24dp);
            }
        }

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!favorite) {
                    int id = idI;
                    int likess = likesI + 1;
                    fab.setImageResource(R.drawable.ic_favorite_black_24dp);
                    likes.setText(String.valueOf(likess));
                    favorite = true;
                    MainActivity.favoriteList.add(id);
                    Snackbar.make(view, "Recipes Added to Favorite", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                    changeData(id, likess);

                } else {
                    int id = idI;
                    int likess = likesI - 1;
                    fab.setImageResource(R.drawable.ic_favorite_border_black_24dp);
                    likes.setText(String.valueOf(likess));
                    favorite = true;
                    MainActivity.favoriteList.remove(new Integer(id));
                    Snackbar.make(view, "Recipes Removed to Favorite", Snackbar.LENGTH_SHORT)
                            .setAction("Action", null).show();
                    changeData(id, likess);
                }

                saveData();


            }
        });

        sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT,
                nameI
                        + "\n\nIngredients :\n--------------------\n"
                        + ingredientsI
                        + "\n\nInstructions :\n--------------------\n"
                        + instructionsI
                        + "\n________________________\nSee more recipient on this app :\n https://play.google.com/store/apps/details?id=com.world.breakfast.breakfast");
        sendIntent.setType("text/*");

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_details, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId()== R.id.action_share){
            startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
        }
        else{
            Intent backhome = new Intent(this, MainActivity.class);
            this.startActivity(backhome);
        }
        return true;
    }


    private void saveData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared perferences", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        Gson gson = new Gson();
        String json = gson.toJson(MainActivity.favoriteList);
        editor.putString("tasks list", json);
        editor.apply();
    }

    private void loadData() {
        SharedPreferences sharedPreferences = getSharedPreferences("shared perferences", MODE_PRIVATE);
        Gson gson = new Gson();
        String json = sharedPreferences.getString("tasks list", null);
        Type type = new TypeToken<ArrayList<Integer>>() {
        }.getType();
        MainActivity.favoriteList = gson.fromJson(json, type);

        if (MainActivity.favoriteList == null) {
            MainActivity.favoriteList = new ArrayList<>();
        }

    }

    public void changeData(int id, int likes) {

        String url = "https://indigestible-repair.000webhostapp.com/addRemFa.php?id=" + id + "&&likes=" + likes;


        AsyncHttpClient client = new AsyncHttpClient();
        client.get(url, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                System.out.println("ok");
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                System.out.println("noOK");
            }
        });
    }

    public void zoomPic(View view) {
        Intent intent = new Intent(this, zoomActivity.class);
        intent.putExtra("image", String.valueOf(intent.getStringExtra("image")));


        intent.putExtra("name", nameI);
        intent.putExtra("image", imageI);
        intent.putExtra("likes", likesI);
        intent.putExtra("id", idI);
        intent.putExtra("instructions", instructionsI);
        intent.putExtra("ingredients", ingredientsI);



        startActivity(intent);

    }
}
