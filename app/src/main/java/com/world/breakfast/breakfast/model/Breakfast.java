package com.world.breakfast.breakfast.model;

import com.google.gson.annotations.SerializedName;

public class Breakfast {
    @SerializedName("id")
    Integer id;
    @SerializedName("likes")
    Integer likes;
    @SerializedName("name")
    String name;
    @SerializedName("ingredients")
    String ingredients;
    @SerializedName("instructions")
    String instructions;
    @SerializedName("image")
    String image;

    public Breakfast(Integer id , String name, String image, Integer likes, String ingredients, String instructions) {
        this.id = id;
        this.likes = likes;
        this.name = name;
        this.ingredients = ingredients;
        this.instructions = instructions;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIngredients() {
        return ingredients;
    }

    public void setIngredients(String ingredients) {
        this.ingredients = ingredients;
    }

    public String getInstructions() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
