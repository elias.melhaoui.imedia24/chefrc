package com.world.breakfast.breakfast.controller;

import android.content.Intent;
import android.os.Bundle;

import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;
import com.world.breakfast.breakfast.R;

import androidx.appcompat.app.AppCompatActivity;

public class zoomActivity extends AppCompatActivity {

    PhotoView photoView;

    String nameI, ingredientsI, instructionsI,imageI;
    int  likesI,idI;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_zoom);

        Intent intent = getIntent();
        nameI =intent.getStringExtra("name");
        ingredientsI = intent.getStringExtra("ingredients");
        instructionsI = intent.getStringExtra("instructions");
        imageI = intent.getStringExtra("image");
        likesI = intent.getIntExtra("likes", 0);
        idI =intent.getIntExtra("id",0);


        photoView = findViewById(R.id.photo_view);
        Picasso.get().load(imageI).into(photoView);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent intent = new Intent(this, DetailsActivity.class);
        intent.putExtra("name", nameI);
        intent.putExtra("image", imageI);
        intent.putExtra("likes", likesI);
        intent.putExtra("id",  idI);
        intent.putExtra("instructions", instructionsI);
        intent.putExtra("ingredients", ingredientsI);
        startActivity(intent);
    }
}
